import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store=new Vuex.Store({
	state:{
		attrVal:[],//detailAttr里的valindex [0,0]
		attrTxt:'请选择商品规格尺寸',
		isLogin:0,
		userinfo:uni.getStorageSync('userinfo')?JSON.parse(uni.getStorageSync('userinfo')):{}
	},
	mutations:{
		setAttr(state,data){
			state.attrVal=data.attrVal
			state.attrTxt=data.attrTxt
		},
		defaultAttr(state){
			state.attrVal=[]
			state.attrTxt='请选择商品规格尺寸'
		},
		login(state,data){
			state.isLogin=1;
		
			state.userinfo=data;
			uni.setStorageSync('userinfo',JSON.stringify(data))
		},
		logout(state){
			state.isLogin=0;
			state.userinfo=[
				
			];
			console.log(state.userinfo)
			uni.setStorageSync('userinfo','')
			uni.setStorageSync('token','')
		},
		setHead(state,data){
			state.userinfo.image=data;
			uni.setStorageSync('userinfo',JSON.stringify(state))
		},
		setNickname(state,data){
			state.userinfo.nickname=data;
			uni.setStorageSync('userinfo',JSON.stringify(state))
		}
		
		
	}
})
export default store;